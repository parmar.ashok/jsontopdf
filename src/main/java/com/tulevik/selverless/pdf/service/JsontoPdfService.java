package com.tulevik.selverless.pdf.service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

@Service
public class JsontoPdfService {

	 @Value("${pdf.path}")
	 String pdfPath;
	 
	public String genratePDF(String strJson) throws FileNotFoundException, DocumentException {
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		//JsonParser jp = JsonParser.parseString​(jsonString).getAsJsonObject();;
		
		Random rand = new Random(); 
		int rand_int1 = rand.nextInt(1000); 
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(pdfPath+"myJSON"+rand_int1+".pdf"));
		Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLUE);
		document.open();
		JsonObject jsonObjectAlt = null;
	//	String prettyJsonString = "";
		if(JsonParser.parseString(strJson).isJsonArray()) {
			 JsonArray jsonArr = JsonParser.parseString(strJson).getAsJsonArray();
			 for (int i = 0; i < jsonArr.size(); i++) {
				 JsonObject jsonObj = jsonArr.get(i).getAsJsonObject();
				 
				 Set<String> keys = jsonObj.keySet();
					Iterator<?> a = keys.iterator();
				    do{
				        String k = a.next().toString();
				       // document.add(new Paragraph(k+" : "+jsonObj.get(k)));
				        Chunk chunk = new Chunk(k+" : "+jsonObj.get(k), font);
				    	Paragraph p1 = new Paragraph(chunk);
				    	document.add(p1);
				    }while(a.hasNext());
			 }
		}else {
			
			
			jsonObjectAlt = JsonParser.parseString(strJson).getAsJsonObject();
			Set<String> keys = jsonObjectAlt.keySet();
			Iterator<?> i = keys.iterator();
		    do{
		        String k = i.next().toString();
		    	
		    	Chunk chunk = new Chunk(k+" : "+jsonObjectAlt.get(k), font);
		    	Paragraph p1 = new Paragraph(chunk);
		    	document.add(p1);
		        
		    }while(i.hasNext());
			
			//prettyJsonString = gson.toJson(jsonObjectAlt);
		}
		
		//JsonElement je = jsonObjectAlt.parse(strJson);
	 	
	

		//document.add(chunk);
		document.close();
		
		
		
		return pdfPath+"myJSON"+rand_int1+".pdf";
	}
}
