package com.tulevik.selverless.pdf.controller;

import java.io.FileNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.itextpdf.text.DocumentException;
import com.tulevik.selverless.pdf.service.JsontoPdfService;


 
@Controller
@RequestMapping("/")
public class JSONController {
	
	 @Autowired
	 JsontoPdfService jsontoPdfService;
	
	 @Value("${pdf.path}")
	private String pdfPath;
	
	@GetMapping("/")
	public String index(HttpSession session,HttpServletRequest request,Model model) {
		System.out.println(" **** "+pdfPath);
		return "/jsontopdf";
	}
 	
	@PostMapping("/getPDF")
	public String getPDF(HttpSession session,HttpServletRequest request,Model model) {
		
		String jsonvalue = request.getParameter("jsonvalue");
		String val = "";
		try {
			val = jsontoPdfService.genratePDF(jsonvalue);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 model.addAttribute("inputjson", jsonvalue);
		 model.addAttribute("res", val);
		return "/jsontopdf";
	}
}
